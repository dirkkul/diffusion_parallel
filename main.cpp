#include "main.hpp"

int main(int argc, char **argv){
	Param par;
	par.nx = 64;
	par.L = 32;
	par.EndTime = 5000;
	par.dt = 0.1;
	par.Dc = 1;
	par.num_threads = 2;
	
	Prepare(&par);
	Scale(&par);
	
	std::vector<float> c(par.nx);
	std::vector<float> c_out(par.nx);
	Initialize(c, &par);
	
	
	for(size_t i = 0; i < par.nx; i++){
		std::cout << c[i]<< " ";
	}
	std::cout << std::endl;
	
	std::thread thr[par.num_threads];
	for(size_t t = 0; t < par.steps; t++){
		for (int tid = 0; tid < par.num_threads; ++tid) {
			thr[tid] = std::thread(Compute, std::ref(c), std::ref(c_out), &par, tid);
		}
		
		for (int tid = 0; tid < par.num_threads; ++tid) {
			thr[tid].join();
		}
		c_out.swap(c);
	}
	
	for(size_t i = 0; i < par.nx; i++){
		std::cout << c[i]<< " ";
	}
	std::cout << std::endl;
	
	return 0;
}

void Compute(std::vector<float>& c, std::vector<float>& c_out, Param* const par, int const tid){
	size_t i = tid;
	while(i < par->nx){
		size_t right = i+1;
		if(right == par->nx) right = 0;
		size_t left = i-1;
		if(left == -1) left = par->nx-1;
		c_out[i] = c[i] + par->Dc* ( c[left] - 2*c[i] + c[right]);
		i += par->num_threads;
	}
}


void Initialize(std::vector<float>& concentration, Param* const par){
	for(size_t x = 0; x < par->nx; x++){
		concentration[x] = 1. + sin(2 * M_PI * x * par->dx/par->L);
	}
}

void Prepare(Param* par){
	par->dx = par->L / par->nx;
	par->steps = (size_t) ceil(par->EndTime/par->dt);
	std::cout << " par->dx" << par->dx << std::endl;
	
}

void Scale(Param* par){
	par->Dc = par->Dc * par->dt/(par->dx*par->dx);
}


